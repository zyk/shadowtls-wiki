# Simple banchmark
Tested under lightsail 1Core 512M VPS(`5.10.0-17-cloud-amd64 #1 SMP Debian 5.10.136-1 (2022-08-13) x86_64 GNU/Linux`).

All cases can reach 100M speed(which is my local ISP limitation) with speedtest.net single connection testing.

Case1:
- Shadowsocks(aes-128-gcm): CPU 19.0%, Mem 1.2%
- ShadowTLS: CPU 5.2%, Mem 0.4%

Case2:
- Shadowsocks(chacha20-ietf-poly1305): CPU 9.7%, Mem 1.4%
- ShadowTLS: CPU 5.2%, Mem 0.5%

Case3:
- Tinc(with TCPOnly): CPU 13.0%, Mem 0.9%

Case4:
- Tinc(with TCPOnly): CPU 29.1%, Mem 0.9%
- ShadowTLS: CPU 24.3%, Mem 0.3%

# Result Analysis
1. Connect latency will be higher. The handshaking cost much time and users should reuse connections.
2. CPU cost is a little bit high I think. The cost comes from 2 sides: memory copy and syscall(including switching), and I guess the memory copy is the biggest reason. Later we will try to implement copy with `splice` or ebpf to save the cost.
3. Since we only have one buffer, we can only read then write on one relay half, which will cause transfer latency becomes bigger. But I think it is not a big problem. We may use double buffer to make it better.
4. It will make tinc cpu goes high and I don't know why...
5. Since we use only 1 connection for benchmark, io_uring may not show big advantage. But for shared proxy infrastructure, it will show better performance.