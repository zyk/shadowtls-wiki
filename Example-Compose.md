## Aboud docker-compose
With a file named as `docker-compose.yml` you can run `docker-compose up -d` or `docker-compose down` in the same folder to start or stop the service.

## Example Compose Files
Here is a sample server(a shadowsocks with a shadow-tls), run it on your remote vps:

```yaml
version: '2.4'
services:
  shadowsocks:
    image: shadowsocks/shadowsocks-libev
    container_name: shadowsocks-raw
    restart: always
    network_mode: "host"
    environment:
      - SERVER_PORT=24000
      - SERVER_ADDR=127.0.0.1
      - METHOD=chacha20-ietf-poly1305
      - PASSWORD=EXAMPLE_PASSWORD_CHANGE_IT
  shadow-tls:
    image: ghcr.io/ihciah/shadow-tls:latest
    restart: always
    network_mode: "host"
    environment:
      - MODE=server
      - LISTEN=0.0.0.0:8443
      - SERVER=127.0.0.1:24000
      - TLS=cloud.tencent.com:443
```
And the client side(you can deploy it in your private network or vps inside the country):

```yaml
version: '2.4'
services:
  shadow-tls:
    image: ghcr.io/ihciah/shadow-tls:latest
    restart: always
    network_mode: "host"
    environment:
      - MODE=client
      - LISTEN=0.0.0.0:3443
      - SERVER=your_vps_ip:8443
      - TLS=cloud.tencent.com
```

Then connect `cn_vps:3443` with shadowsocks protocol on your mobile phones or PCs will work.

## How to Choose TLS Server
1. Low latency: Test on your vps to make sure the destination is with low latency. You can do the checking with ping for simple, but with curl will be more accurate.
2. Trusted by the firewall: It must be a legal service, and widely used.